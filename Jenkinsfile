def PROJ_NAME='advantage-customer-communication-service-utility'

def SSL_OPTS = '-Djavax.net.ssl.trustStore=/opt/oracle/certs/cacerts+mason -Djavax.net.ssl.trustStorePassword=changeit'
def SONAR_OPTS

def artifactServer = Artifactory.server "mason-artifactory"
def rtMaven = Artifactory.newMavenBuild()
def buildInfo
def publishToArtifactory = false

pipeline {
	agent any

	environment { 
		ORACLE_HOME = '/opt/oracle/soa12c'
		SONAR_USER_HOME = "${HOME}/.sonar"
		SONAR_SCANNER_OPTS = "${SSL_OPTS}"
	}
	
	options {
		skipDefaultCheckout()
	}

	stages {
		stage('Initialize') {
			agent {label 'msn-build'}
			steps {
				script {
					// clean workspace
					cleanWs()
					
					// Checkout source code
					dir (".data/jenkins/${PROJ_NAME}") {
						checkout scm
					}
					
					// define some needed variables
					def pom = readMavenPom file: ".data/jenkins/${PROJ_NAME}/pom.xml"
					def resolvedPomVersion =  "${pom.version}".replace('${build.number}', "${BUILD_NUMBER}")
					SONAR_OPTS = "-Dsonar.projectKey=${pom.groupId}:${pom.artifactId} -Dsonar.projectVersion=${resolvedPomVersion}, -Dsonar.projectName='${pom.description}' -Dsonar.sources=.data/jenkins/${PROJ_NAME}"
					
					// Setup Artifactory and Maven tools
					rtMaven.tool = "M3"
					rtMaven.resolver releaseRepo:'libs-release', snapshotRepo:'libs-snapshot', server: artifactServer
					rtMaven.deployer releaseRepo:'libs-release-local', snapshotRepo:'libs-snapshot-local', server: artifactServer
					rtMaven.deployer.deployArtifacts = false
					rtMaven.opts = "-Dbuild.number=${BUILD_NUMBER} ${SSL_OPTS}"
					
					// Don't want to publish release artifact when POM indicates a snapshot
					def pomVerIsRelease = resolvedPomVersion.indexOf('-SNAPSHOT') == -1
					
					if (pomVerIsRelease && !BRANCH_NAME.startsWith('release/') && !BRANCH_NAME.startsWith('hotfix/')) {
						publishToArtifactory = false
					} else {
						publishToArtifactory = true
					}
				}
			}
		}
		
		
		stage('SonarQube Analysis') {
			agent {label 'msn-build'}
			steps {
				script {
					def scannerHome = tool 'SonarQubeScanner'
					withSonarQubeEnv('SonarQubeServer') {
						sh "${scannerHome}/bin/sonar-scanner -Dsonar.host.url=${SONAR_HOST_URL} -Dsonar.login=${SONAR_AUTH_TOKEN} ${SONAR_OPTS} "
					}
				}
			}
		}
		
		stage('Unit Test') {
			agent {label 'msn-build'}
			steps {
				script {
					dir (".data/jenkins/${PROJ_NAME}") {
						rtMaven.run pom: 'pom.xml', goals: 'clean test'
					}
				}
			}
		}
		
		stage('Build') {
			agent {label 'msn-build'}
			steps {
				script {
					dir (".data/jenkins/${PROJ_NAME}") {
						rtMaven.opts = rtMaven.opts + '-DskipTests'
						buildInfo = rtMaven.run pom: 'pom.xml', goals: 'clean install'
					}
				}
			}
		}
		
		stage('SonarQube Quality Gate') {
			// no agent
			steps {
				timeout(time: 1, unit: 'HOURS') {
					waitForQualityGate abortPipeline: true
				}
			}
		}
		
		stage('Publish and Archive - Snapshot') {
			agent {label 'msn-build'}
			when {
				expression {publishToArtifactory}
				not {
					branch 'release/*'
				}
			}
			steps {
				script{
					dir (".data/jenkins/${PROJ_NAME}") {
						artifactServer.publishBuildInfo buildInfo
						rtMaven.deployer.deployArtifacts buildInfo
					}
				}
				
			}
		}
		
		stage('Publish and Archive') {
			agent {label 'msn-build'}
			when {
				expression {publishToArtifactory}
				branch 'release/*'
			}
			steps {
				script{
					dir (".data/jenkins/${PROJ_NAME}") {
						artifactServer.publishBuildInfo buildInfo
						rtMaven.deployer.deployArtifacts buildInfo
					}
				}
				
			}
		}
	}
	
	post {
        always {
			node('msn-build') {
				junit ".data/jenkins/${PROJ_NAME}/.data/maven/jasmine/*.xml"
			}
            
        }
    }
}
